#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include "SFML/Network.hpp"
#include "cHeader.h"

std::vector<std::string> headerSplit(std::string& header);

std::string timeRefresh();

void receiveMsg(sf::TcpSocket &socket, Header &header);

void sendMsg(sf::TcpSocket &socket, Header &header);