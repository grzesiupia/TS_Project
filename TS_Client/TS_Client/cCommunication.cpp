#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include "SFML/Network.hpp"
#include "cHeader.h"
#include "cCommunication.h"

void sendMsg(sf::TcpSocket &socket, Header &header) {
	
	std::string headerString;
	std::string data, toLower;
	
	
	while (header.operation != "FIN") {
		std::string time = timeRefresh();
		if (header.identN == "1" || header.identN == "2") {
			std::getline(std::cin, data);
			toLower = data;
			std::for_each(toLower.begin(), toLower.end(), [](char& c) {
				c = ::tolower(c);
				});
			if (toLower == "disconnect") {
				header.operation = "FIN";
				data.clear();
			}
			if (toLower == "invite") {
				header.operation = "INV";
				data.clear();
			}
			if (toLower == "clients") {
				header.operation = "CHECK";
				data.clear();
			}
			if (toLower == "exit") {
				exit(1);
			}

		}

		headerString = "Identyfikator-)" + header.identN + "(|Operacja-)" + header.operation + "(|Status-)" + header.status + "(|CurrT-)" + time + "(|Data-)" + data + "(|";
		socket.send(headerString.c_str(), headerString.length());
		if (data.length() > 0) {
			std::cout << "[" << time << "] You said:" << data << std::endl;
		}
	}
}

void receiveMsg(sf::TcpSocket &socket, Header &header) {
	
	std::string time = timeRefresh();
	std::size_t recived;
	char buffer[1024];
	std::vector<std::string>tokens;
	std::string recivedHeader;
	
	do {
		
		socket.receive(buffer, sizeof(buffer), recived);

		if (recived > 0) {
			for (int i = 0; i < recived; i++) {
				recivedHeader.push_back(buffer[i]);
			}
			tokens = headerSplit(recivedHeader);

			if (tokens[0] == "1" && tokens[1] == "GIVEID") {
				header.identN = "1";
				header.status = "OK";
				std::cout << "WELCOME TO THE SERVER" << std::endl;
			}
			else if (tokens[0] == "2" && tokens[1] == "GIVEID") {
				header.identN = "2";
				header.status = "OK";
				std::cout << "WELCOME TO THE SERVER" << std::endl;
			}
			if (tokens[1] == "FIN") {
				std::cout << "[" << tokens[3] << "] SERVER: YOU HAVE BEED DISCONNECTED ('EXIT' TO CLOSE THE WINDOW)" << std::endl;
				socket.disconnect();
			}
			if (tokens[1] == "FIN2") {
				std::cout << "[" << tokens[3] << "] SERVER: YOUR FRIEND HAS DISCONNECTED" << std::endl;
				std::cout << "[" << tokens[3] << "] ('EXIT' TO CLOSE THE WINDOW)" << std::endl;
				socket.disconnect();
			}
			if (tokens[1] == "INV") {
				char yn;
				bool asw = false;
				std::cout << "Do you want to start conversation?(y/n): ";
				do {
					std::cin >> yn;
					switch (yn) {
					case 'y':
						header.operation = "ACC";
						asw = true;
						break;
					case 'n':
						header.operation = "DEC";
						asw = true;
						break;
					default:
						std::cout << "WRONG ANSWER CHOOSE Y OR N." << std::endl;
					}
				} while (asw == false);

				std::string headerString = "Identyfikator-)" + header.identN + "(|Operacja-)" + header.operation + "(|Status-)" + header.status + "(|CurrT-)" + time + "(|Data-)""(|";

				socket.send(headerString.c_str(), headerString.length());
			}
			if (tokens[1] == "ACC") {
				std::cout << "Invitation accepted. you can start conversation." << std::endl;
				header.operation = "MSG";
			}
			if (tokens[1] == "DEC") {
				std::cout << "Invitation declined. you can try again." << std::endl;
			}
			if (tokens[1] == "MSG") {
				header.operation = "MSG";
				std::cout << "[" << tokens[3] << "] Friend said: " << tokens[4] << std::endl;
			}
		}
	} while (tokens[1] != "FIN");
}

std::vector<std::string> headerSplit(std::string& header) {
	std::vector<std::string>tokens;
	std::string delimiter = "(|";
	size_t pos = 0;
	std::string token;

	while ((pos = header.find(delimiter)) != std::string::npos) {
		token = header.substr(0, pos);
		tokens.push_back(token);
		header.erase(0, pos + delimiter.length());
	}

	delimiter = "-)";

	for (int i = 0; i < tokens.size(); i++) {
		pos = tokens[i].find(delimiter);
		tokens[i].erase(0, pos + delimiter.length());
	}

	return tokens;
}

std::string timeRefresh() {
	time_t rawtime;
	struct tm* timeinfo;
	char buffertime[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffertime, sizeof(buffertime), "%d-%m-%Y %H:%M:%S", timeinfo);
	std::string strTime(buffertime);

	return strTime;
}