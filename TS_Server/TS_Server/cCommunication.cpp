#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <ctime>
#include "SFML/Network.hpp"
#include "cHeader.h"
#include "cCommunication.h"

/*
Identyfikator = tokens[0]
Operacja = tokens[1]
Status = tokens[2]
Czas = tokens[3]
Dane = tokens[4]
*/

void reciveMsg(sf::TcpSocket& socket1, sf::TcpSocket& socket2, Header &header, std::string &recivedHeader) {

	
	std::size_t recived;
	bool isConnected = false;
	std::string headerString;
	std::string toToken;

	std::vector<std::string> tokens;
	std::string napis;
	char buffer[1024];

	do{
		std::string time = timeRefresh();
		recivedHeader.clear();

		socket1.receive(buffer, sizeof(buffer), recived);
		if (recived > 0) {
			for (int i = 0; i < recived; i++) {
				recivedHeader.push_back(buffer[i]);
			}
			toToken = recivedHeader;
			tokens = headerSplit(toToken);

			if (tokens[1] == "GETID" && tokens[0] == "0" && isConnected == false) {
				header.identN = "1";
				header.operation = "GIVEID";
				header.status = "OK";
				isConnected = true;
				napis = "Identyfikator-)" + header.identN + "(|Operacja-)" + header.operation + "(|Status-)" + header.status + "(|CurrT-)" + time + "(|Data-)""(|";
				socket1.send(napis.c_str(), napis.length());
			}
			else if (tokens[1] == "GETID" && tokens[0] == "0" && isConnected == true) {
				header.identN = "2";
				header.operation = "GIVEID";
				header.status = "OK";
				napis = "Identyfikator-)" + header.identN + "(|Operacja-)" + header.operation + "(|Status-)" + header.status + "(|CurrT-)" + time + "(|Data-)""(|";
				socket1.send(napis.c_str(), napis.length());
			}
			else if (tokens[1] == "FIN") {
				header.operation = "FIN";
				napis = "Identyfikator-)" + header.identN + "(|Operacja-)" + header.operation + "(|Status-)" + header.status + "(|CurrT-)" + time + "(|Data-)""(|";
				socket1.send(napis.c_str(), napis.length());
				header.operation = "FIN2";
				socket1.disconnect();
				napis = "Identyfikator-)" + header.identN + "(|Operacja-)" + header.operation + "(|Status-)" + header.status + "(|CurrT-)" + time + "(|Data-)""(|";
				socket2.send(napis.c_str(), napis.length());
			}
			else if (tokens[1] == "CHECK") {
				header.operation = "CHECK";

			}
			else {
				napis = recivedHeader;
				socket2.send(napis.c_str(), napis.length());
			}
		}
	} while (tokens[1] != "FIN");
}


std::vector<std::string> headerSplit(std::string& header) {
	std::vector<std::string>tokens;
	std::string delimiter = "(|";
	size_t pos = 0;
	std::string token;
	while ((pos = header.find(delimiter)) != std::string::npos) {
		token = header.substr(0, pos);
		tokens.push_back(token);
		header.erase(0, pos + delimiter.length());
	}
	delimiter = "-)";
	for (int i = 0; i < tokens.size(); i++) {
		pos = tokens[i].find(delimiter);
		tokens[i].erase(0, pos + delimiter.length());
	}

	return tokens;
}

std::string timeRefresh() {
	time_t rawtime;
	struct tm* timeinfo;
	char buffertime[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffertime, sizeof(buffertime), "%d-%m-%Y %H:%M:%S", timeinfo);
	std::string strTime(buffertime);

	return strTime;
}