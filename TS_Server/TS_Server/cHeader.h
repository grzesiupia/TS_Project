#pragma once
#include <string>

struct Header {
	std::string identN;
	std::string status;
	std::string operation;
};